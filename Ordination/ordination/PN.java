package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {

	public PN(LocalDate startDen, LocalDate slutDen, double antalEnheder) {
		super(startDen, slutDen);
		this.antalEnheder = antalEnheder; 
	}

	private double antalEnheder;
	private ArrayList<LocalDate> datoer = new ArrayList<>();
	
	public ArrayList<LocalDate> getDatoer() {
		return new ArrayList<>(datoer);
	}
	
	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		if (givesDen.compareTo(getStartDen()) >= 0 && givesDen.compareTo(getSlutDen()) <= 0) {
			datoer.add(givesDen);
			return true;
		} else {
			return false;
		}
	}

	public double doegnDosis() {

		if (datoer.isEmpty()) {
			return 0;
		}
		LocalDate[] firstAndLast = findFirstAndLastDates();

		return samletDosis() / (ChronoUnit.DAYS.between(firstAndLast[0], firstAndLast[1]) + 1);

	}


	private LocalDate[] findFirstAndLastDates() {
		if (datoer.isEmpty()) {
			return null;
		}

		LocalDate[] firstLast = new LocalDate[2];
		firstLast[0] = datoer.get(0);
		firstLast[1] = datoer.get(0);

		for (LocalDate date : datoer) {
			if (date.isBefore(firstLast[0])) {
				firstLast[0] = date;
			} else if (date.isAfter(firstLast[1])) {
				firstLast[1] = date;
			}
		}

		return firstLast;

	}

	public double samletDosis() {
		return antalEnheder * datoer.size();
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {
		return datoer.size();
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {
		return "Pro necesare";
	}

}
