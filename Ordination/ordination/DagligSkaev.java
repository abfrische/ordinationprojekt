package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	public DagligSkaev(LocalDate startDen, LocalDate slutDen) {
		super(startDen, slutDen);
		// TODO Auto-generated constructor stub
	}

	private ArrayList<Dosis> doser = new ArrayList<>();

	public ArrayList<Dosis> getDoser() {
		return new ArrayList<>(doser);
	}

	public Dosis createDosis(LocalTime tid, double antal) {
		Dosis dosis = new Dosis(tid, antal);
		doser.add(dosis);
		return dosis;
	}

	public void removeDosis(Dosis dosis) {
		if (doser.contains(dosis)) {
			doser.remove(dosis);
		}
	}

	@Override
	public double samletDosis() {
		return doegnDosis() * antalDage();
   
	}

	@Override
	public double doegnDosis() {
		double sum = 0;
		for (Dosis d : doser) {
			sum += d.getAntal();
		}
		return sum;
	}

	@Override
	public String getType() {
		return "Daglig Skæv";
	}
}
