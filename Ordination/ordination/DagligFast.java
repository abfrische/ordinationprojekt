package ordination;

import java.time.*;
import java.util.Arrays;

public class DagligFast extends Ordination {
	
	public DagligFast(LocalDate startDen, LocalDate slutDen) {
		super(startDen, slutDen);
	}

	private Dosis[] doser = new Dosis[4];
	
	public Dosis[] getDoser() {
		return Arrays.copyOf(doser, doser.length);
	}
	
	public void addDosis(int i, double antal) {
		doser[i] = new Dosis(LocalTime.of(((i+1)*6)%24, 0), antal);
	}
	
	public void removeDosis(int i) {
		doser[i] = null;
	}

	@Override
	public double samletDosis() {
		return doegnDosis() * antalDage();	
	}

	@Override
	public double doegnDosis() {
		double sum = 0;
		for(Dosis d : doser) {
			if(d != null) {
				sum += d.getAntal();
			}
		}
		return sum;
	}

	@Override
	public String getType() {
		return "Daglig Fast";
	}
	
}