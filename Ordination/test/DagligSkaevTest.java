package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;

public class DagligSkaevTest {

	DagligSkaev ds = new DagligSkaev(LocalDate.of(2020, 03, 03), LocalDate.of(2020, 04, 03));

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testDoegnDosis() {
		ds.createDosis(LocalTime.of(12, 00), 1);
		assertEquals(1, ds.doegnDosis(), 0.01);
		ds.createDosis(LocalTime.of(12, 00), 3);
		ds.createDosis(LocalTime.of(13, 00), 2);
		ds.createDosis(LocalTime.of(14, 00), 1);
		ds.createDosis(LocalTime.of(15, 00), 3);
		ds.createDosis(LocalTime.of(16, 00), 2);
		ds.createDosis(LocalTime.of(17, 00), 1);
		assertEquals(13, ds.doegnDosis(), 0.01);

	}

	@Test
	public void testDoegnDosis2() {
		ds.createDosis(LocalTime.of(12, 00), 1.2);
		ds.createDosis(LocalTime.of(13, 00), 3.1);
		ds.createDosis(LocalTime.of(14, 00), 4.1);
		assertEquals(8.4, ds.doegnDosis(), 0.01);
	}

	@Test
	public void testSamletDosis() {
		DagligSkaev ds1 = new DagligSkaev(LocalDate.of(2020, 03, 03), LocalDate.of(2020, 03, 03));
		ds1.createDosis(LocalTime.of(12, 00), 1);
		ds1.createDosis(LocalTime.of(13, 00), 2);
		ds1.createDosis(LocalTime.of(14, 00), 1.5);
		ds1.createDosis(LocalTime.of(15, 00), 3);
		ds1.createDosis(LocalTime.of(16, 00), 1);
		assertEquals(8.5, ds1.samletDosis(), 0.01);
	}
	@Test
	public void testSamletDosis2() {
		DagligSkaev ds2 = new DagligSkaev(LocalDate.of(2020, 03, 03), LocalDate.of(2020, 03, 07));
		ds2.createDosis(LocalTime.of(12, 00), 1);
		ds2.createDosis(LocalTime.of(13, 00), 2);
		ds2.createDosis(LocalTime.of(14, 00), 1.5);
		ds2.createDosis(LocalTime.of(15, 00), 3);
		ds2.createDosis(LocalTime.of(16, 00), 1);
		assertEquals(42.5, ds2.samletDosis(), 0.01);
	}

}
