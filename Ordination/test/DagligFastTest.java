package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;

public class DagligFastTest {

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void testdoegnDosis() {
		DagligFast tc1 = new DagligFast(LocalDate.of(2020, 3, 03), LocalDate.of(2020, 3, 10));
		tc1.addDosis(0, 1);
		tc1.addDosis(1, 3);
		tc1.addDosis(2, 2);
		tc1.addDosis(3, 1);
		assertEquals(7, tc1.doegnDosis(), .001);

		DagligFast tc2 = new DagligFast(LocalDate.of(2020, 3, 03), LocalDate.of(2020, 3, 10));
		tc2.addDosis(0, 1);
		tc2.addDosis(1, 0);
		tc2.addDosis(2, 3);
		tc2.addDosis(3, 2);
		assertEquals(6, tc2.doegnDosis(), .001);
	}

	@Test
	public void testSamletDosis() {
		DagligFast tc1 = new DagligFast(LocalDate.of(2020, 3, 02), LocalDate.of(2020, 3, 02));
		tc1.addDosis(0, 1);
		tc1.addDosis(1, 3);
		tc1.addDosis(2, 2);
		tc1.addDosis(3, 1);
		assertEquals(7, tc1.samletDosis(), .001);

		DagligFast tc2 = new DagligFast(LocalDate.of(2020, 3, 02), LocalDate.of(2020, 3, 10));
		tc2.addDosis(0, 1);
		tc2.addDosis(1, 3);
		tc2.addDosis(2, 2);
		tc2.addDosis(3, 1);
		assertEquals(63, tc2.samletDosis(), .001);
	}

}
