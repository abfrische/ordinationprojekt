package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import controller.Controller;
import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Dosis;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class ControllerTest {

	private Controller tester;
	private Patient patient = new Patient("121256-0512", "Jane Jensen", 63.4);
	private Laegemiddel laegemiddel = new Laegemiddel("Acetylsalisylsyre", 0.1, 1.5, 2, "Styk");
	private LocalTime[] klokkeslet = new LocalTime[] { LocalTime.of(12, 00) };
	private double[] antalEnheder = new double[] { 1.0 };

	@Before
	public void setUp() throws Exception {
		tester = Controller.getTestController();
		patient = new Patient("121256-0512", "Jane Jensen", 63.4);
	}

	@Test
	public void testOpretPNOrdination() {
		PN pn = tester.opretPNOrdination(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 2), patient, laegemiddel, 2);
		assertNotNull(pn);
		assertEquals(laegemiddel, pn.getLaegemiddel());
		assertTrue(patient.getOrdinationer().contains(pn));

		pn = tester.opretPNOrdination(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 4), patient, laegemiddel, 2);
		assertNotNull(pn);
		assertEquals(laegemiddel, pn.getLaegemiddel());
		assertTrue(patient.getOrdinationer().contains(pn));

		pn = tester.opretPNOrdination(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 4), patient, null, 2);
		assertNotNull(pn);
		assertNull(pn.getLaegemiddel());
		assertTrue(patient.getOrdinationer().contains(pn));

		pn = tester.opretPNOrdination(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 4), patient, laegemiddel, 1);
		assertNotNull(pn);
		assertEquals(laegemiddel, pn.getLaegemiddel());
		assertTrue(patient.getOrdinationer().contains(pn));
	}

	@Test
	public void testOpretPNOrdinationUgyldigDato() {
		try {
			tester.opretPNOrdination(LocalDate.of(2020, 3, 4), LocalDate.of(2020, 3, 2), patient, laegemiddel, 1);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("startDato skal være før slutDato", e.getMessage());
		}
	}

	@Test
	public void testOpretPNOrdinationPatientNull() {
		try {
			tester.opretPNOrdination(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 4), null, laegemiddel, 1);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Patient må ikke være null", e.getMessage());
		}
	}

	@Test
	public void testOpretPNOrdinationAntalNul() {
		try {
			tester.opretPNOrdination(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 4), patient, laegemiddel, 0);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Antal skal være større end 0", e.getMessage());
		}
	}

	@Test
	public void testOpretDagligFastOrdination() {
		DagligFast df = tester.opretDagligFastOrdination(LocalDate.of(2020, 03, 02), LocalDate.of(2020, 03, 02),
				patient, laegemiddel, 1, 1, 1, 1);
		assertNotNull(df);
		assertEquals(laegemiddel, df.getLaegemiddel());
		assertTrue(patient.getOrdinationer().contains(df));
		Dosis[] doser = df.getDoser();
		assertEquals(1, doser[0].getAntal(), 0.01);
		assertEquals(1, doser[1].getAntal(), 0.01);
		assertEquals(1, doser[2].getAntal(), 0.01);
		assertEquals(1, doser[3].getAntal(), 0.01);

		df = tester.opretDagligFastOrdination(LocalDate.of(2020, 03, 02), LocalDate.of(2020, 03, 04), patient,
				laegemiddel, 1, 1, 1, 1);
		assertNotNull(df);
		assertEquals(laegemiddel, df.getLaegemiddel());
		assertTrue(patient.getOrdinationer().contains(df));

		df = tester.opretDagligFastOrdination(LocalDate.of(2020, 03, 02), LocalDate.of(2020, 03, 04), patient, null, 1,
				1, 1, 1);
		assertNotNull(df);
		assertNull(df.getLaegemiddel());
		assertTrue(patient.getOrdinationer().contains(df));

		df = tester.opretDagligFastOrdination(LocalDate.of(2020, 03, 02), LocalDate.of(2020, 03, 04), patient,
				laegemiddel, 1, 0, 0, 0);
		assertNotNull(df);
		assertEquals(laegemiddel, df.getLaegemiddel());
		assertTrue(patient.getOrdinationer().contains(df));
		doser = df.getDoser();
		assertEquals(1, doser[0].getAntal(), 0.01);
		assertNull(doser[1]);
		assertNull(doser[2]);
		assertNull(doser[3]);
	}

	@Test
	public void testOpretDagligFastOrdinationUgyldigDato() {
		try {
			tester.opretDagligFastOrdination(LocalDate.of(2020, 3, 4), LocalDate.of(2020, 3, 2), patient, laegemiddel,
					1, 1, 1, 1);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Start dato kan ikke være efter slut dato", e.getMessage());
		}
	}

	@Test
	public void testOpretDagligFastOrdinationPatientNull() {
		try {
			tester.opretDagligFastOrdination(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 4), null, laegemiddel, 1,
					1, 1, 1);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Patient kan ikke være null", e.getMessage());
		}
	}

	@Test
	public void testOpretDagligFastOrdinationAntalNul() {
		try {
			tester.opretDagligFastOrdination(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 4), patient, laegemiddel,
					0, 0, 0, 0);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Mindst en af antal skal være > 0", e.getMessage());
		}
	}

	@Test
	public void testOpretDagligFastOrdinationAntalUnderNul() {
		try {
			tester.opretDagligFastOrdination(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 4), patient, laegemiddel,
					1, 1, -1, 1);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Antal må ikke være < 0", e.getMessage());
		}
	}

	@Test
	public void testOpretDagligSkaevOrdination() {
		ArrayList<Dosis> doser;

		DagligSkaev ds = tester.opretDagligSkaevOrdination(LocalDate.of(2020, 03, 02), LocalDate.of(2020, 03, 02),
				patient, laegemiddel, klokkeslet, antalEnheder);
		assertNotNull(ds);
		assertEquals(laegemiddel, ds.getLaegemiddel());
		assertTrue(patient.getOrdinationer().contains(ds));
		doser = ds.getDoser();
		for (int i = 0; i < klokkeslet.length; i++) {
			assertEquals(doser.get(i).getAntal(), antalEnheder[i], 0.01);
			assertEquals(doser.get(i).getTid(), klokkeslet[i]);
		}

		ds = tester.opretDagligSkaevOrdination(LocalDate.of(2020, 03, 02), LocalDate.of(2020, 03, 04), patient,
				laegemiddel, klokkeslet, antalEnheder);
		assertNotNull(ds);

		ds = tester.opretDagligSkaevOrdination(LocalDate.of(2020, 03, 02), LocalDate.of(2020, 03, 04), patient, null,
				klokkeslet, antalEnheder);
		assertNotNull(ds);
		assertNull(ds.getLaegemiddel());

		LocalTime[] klokkeslet2 = new LocalTime[] { LocalTime.of(14, 00) };
		ds = tester.opretDagligSkaevOrdination(LocalDate.of(2020, 03, 02), LocalDate.of(2020, 03, 02), patient,
				laegemiddel, klokkeslet2, antalEnheder);
		doser = ds.getDoser();
		for (int i = 0; i < klokkeslet2.length; i++) {
			assertEquals(doser.get(i).getTid(), klokkeslet2[i]);
		}

		double[] antalEnheder2 = new double[] { 4 };
		ds = tester.opretDagligSkaevOrdination(LocalDate.of(2020, 03, 02), LocalDate.of(2020, 03, 02), patient,
				laegemiddel, klokkeslet, antalEnheder2);
		doser = ds.getDoser();
		for (int i = 0; i < antalEnheder2.length; i++) {
			assertEquals(doser.get(i).getAntal(), antalEnheder2[i], 0.01);
		}

		klokkeslet2 = new LocalTime[] { LocalTime.of(12, 00), LocalTime.of(14, 00) };
		antalEnheder2 = new double[] { 1, 4 };
		ds = tester.opretDagligSkaevOrdination(LocalDate.of(2020, 03, 02), LocalDate.of(2020, 03, 02), patient,
				laegemiddel, klokkeslet2, antalEnheder2);
		doser = ds.getDoser();
		for (int i = 0; i < klokkeslet2.length; i++) {
			assertEquals(doser.get(i).getTid(), klokkeslet2[i]);
			assertEquals(doser.get(i).getAntal(), antalEnheder2[i], 0.01);
		}
	}

	@Test
	public void testOpretDagligSkaevOrdinationUgyldigDato() {
		try {
			tester.opretDagligSkaevOrdination(LocalDate.of(2020, 3, 4), LocalDate.of(2020, 3, 2), patient, laegemiddel,
					klokkeslet, antalEnheder);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("startDato skal være før slutDato", e.getMessage());
		}
	}

	@Test
	public void testOpretDagligSkaevOrdinationPatientNull() {
		try {
			tester.opretDagligSkaevOrdination(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 4), null, laegemiddel,
					klokkeslet, antalEnheder);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Patient må ikke være null", e.getMessage());
		}
	}
	
	@Test
	public void testOpretDagligSkaevOrdinationKlokkesletOgAntalEnhederTom() {
		try {
			tester.opretDagligSkaevOrdination(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 4), patient, laegemiddel,
					new LocalTime[0], new double[0]);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Klokkeslet og AntalEnheder må ikke være tomt", e.getMessage());
		}
	}

	@Test
	public void testOpretDagligSkaevOrdinationAntalEnhederIkkeLigKlokkeslet() {
		try {
			tester.opretDagligSkaevOrdination(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 4), null, laegemiddel,
					klokkeslet, new double[2]);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("antal af tider og enheder passer ikke", e.getMessage());
		}
	}
	
	@Test
	public void testOrdinationPNAnvendt() {
		PN pn = new PN(LocalDate.of(2020, 03, 2), LocalDate.of(2020, 3, 8), 2);
		
		int antalDatoer = pn.getDatoer().size();
		tester.ordinationPNAnvendt(pn, LocalDate.of(2020, 3, 2));
		assertEquals(antalDatoer+1, pn.getDatoer().size());
		
		antalDatoer = pn.getDatoer().size();
		tester.ordinationPNAnvendt(pn, LocalDate.of(2020, 3, 5));
		assertEquals(antalDatoer+1, pn.getDatoer().size());
		
		antalDatoer = pn.getDatoer().size();
		tester.ordinationPNAnvendt(pn, LocalDate.of(2020, 3, 8));
		assertEquals(antalDatoer+1, pn.getDatoer().size());
	}
	
	@Test
	public void testOrdinationPNAnvendtForTidligDato() {
		PN pn = new PN(LocalDate.of(2020, 03, 2), LocalDate.of(2020, 3, 8), 2);
		
		try {
			tester.ordinationPNAnvendt(pn, LocalDate.of(2020, 3, 1));
		} catch (IllegalArgumentException e) {
			assertEquals("Dato er inden ordinationens dato", e.getMessage());
		}
	}
	
	@Test
	public void testOrdinationPNAnvendtForSenDato() {
		PN pn = new PN(LocalDate.of(2020, 03, 2), LocalDate.of(2020, 3, 8), 2);
		
		try {
			tester.ordinationPNAnvendt(pn, LocalDate.of(2020, 3, 9));
		} catch (IllegalArgumentException e) {
			assertEquals("Dato er efter ordinationens dato", e.getMessage());
		}
	}
	
	@Test
	public void testAnbefaletDosisPrDoegn() {
		patient.setVaegt(15);
		assertEquals(1.5, tester.anbefaletDosisPrDoegn(patient, laegemiddel), 0.001);
		patient.setVaegt(24.99);
		assertEquals(2.5, tester.anbefaletDosisPrDoegn(patient, laegemiddel), 0.001);
		patient.setVaegt(25);
		assertEquals(37.5, tester.anbefaletDosisPrDoegn(patient, laegemiddel), 0.001);
		patient.setVaegt(60);
		assertEquals(90, tester.anbefaletDosisPrDoegn(patient, laegemiddel), 0.001);
		patient.setVaegt(120);
		assertEquals(180, tester.anbefaletDosisPrDoegn(patient, laegemiddel), 0.001);
		patient.setVaegt(120.01);
		assertEquals(240.02, tester.anbefaletDosisPrDoegn(patient, laegemiddel), 0.001);
		patient.setVaegt(150);
		assertEquals(300, tester.anbefaletDosisPrDoegn(patient, laegemiddel), 0.001);
	}
	
	@Test
	public void testAntalOrdinationPrVægtPrLægemiddel() {
		Patient p1 = tester.opretPatient("1234567890", "Bob", 14);
		tester.opretPNOrdination(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 8), p1, laegemiddel, 2);
		Patient p2 = tester.opretPatient("1234567891", "Troels", 26);
		tester.opretPNOrdination(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 8), p2, laegemiddel, 2);
		Patient p3 = tester.opretPatient("1234567892", "Henning", 100);
		tester.opretPNOrdination(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 8), p3, laegemiddel, 2);
		
		Laegemiddel fucidin = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		Patient p4 = tester.opretPatient("1234567893", "Dorthe", 120);
		tester.opretPNOrdination(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 8), p4, fucidin, 2);
		
		assertEquals(3, tester.antalOrdinationerPrVægtPrLægemiddel(10, 110, laegemiddel));
		assertEquals(3, tester.antalOrdinationerPrVægtPrLægemiddel(14, 110, laegemiddel));
		assertEquals(2, tester.antalOrdinationerPrVægtPrLægemiddel(14.01, 110, laegemiddel));
		assertEquals(2, tester.antalOrdinationerPrVægtPrLægemiddel(10, 99.99, laegemiddel));
		assertEquals(3, tester.antalOrdinationerPrVægtPrLægemiddel(10, 100, laegemiddel));
		assertEquals(3, tester.antalOrdinationerPrVægtPrLægemiddel(10, 130, laegemiddel));
		assertEquals(1, tester.antalOrdinationerPrVægtPrLægemiddel(100, 130, fucidin));
	}
}
