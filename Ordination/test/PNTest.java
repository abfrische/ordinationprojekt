package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import controller.Controller;
import ordination.PN;

public class PNTest {
	
	private PN pn;
	
	@Before
	public void setUp() throws Exception {
		pn = new PN(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 10), 2);
	}
	
	// findFirstAndLastDates i PN er privat og må laves public når den skal testes. Ellers må testen udkommenteres.
	
//	@Test
//	public void testFindFirstAndLastDates() {		
//		assertNull(pn.findFirstAndLastDates());
//		
//		pn.givDosis(LocalDate.of(2020, 3, 2));
//		LocalDate[] firstAndLast = pn.findFirstAndLastDates();
//		assertEquals(LocalDate.of(2020, 3, 2), firstAndLast[0]);
//		assertEquals(LocalDate.of(2020, 3, 2), firstAndLast[1]);
//		
//		pn.givDosis(LocalDate.of(2020, 3, 3));
//		pn.givDosis(LocalDate.of(2020, 3, 4));
//		
//		firstAndLast = pn.findFirstAndLastDates();
//		assertEquals(LocalDate.of(2020, 3, 2), firstAndLast[0]);
//		assertEquals(LocalDate.of(2020, 3, 4), firstAndLast[1]);
//	}
	
	@Test
	public void testSamletDosis() {
		assertEquals(0, pn.samletDosis(), .001);
		
		pn.givDosis(LocalDate.of(2020, 3, 2));
		assertEquals(2, pn.samletDosis(), .001);
		
		pn.givDosis(LocalDate.of(2020, 3, 3));
		pn.givDosis(LocalDate.of(2020, 3, 4));
		assertEquals(6, pn.samletDosis(), .001);
	}
	
	@Test
	public void testDoegnDosis() {
		assertEquals(0, pn.doegnDosis(), 0.001);
		
		pn.givDosis(LocalDate.of(2020, 3, 2));
		assertEquals(2, pn.doegnDosis(), 0.001);
		
		pn.givDosis(LocalDate.of(2020, 3, 5));
		assertEquals(1, pn.doegnDosis(), 0.001);
		
		pn = new PN(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 10), 2);
		pn.givDosis(LocalDate.of(2020, 3, 2));
		pn.givDosis(LocalDate.of(2020, 3, 3));
		pn.givDosis(LocalDate.of(2020, 3, 4));
		assertEquals(2, pn.doegnDosis(), 0.001);
	}
	
	@Test
	public void testGivDosis() {
		pn = new PN(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 31), 2);
		assertFalse(pn.givDosis(LocalDate.of(2020, 3, 1)));
		assertTrue(pn.givDosis(LocalDate.of(2020, 3, 2)));
		assertTrue(pn.givDosis(LocalDate.of(2020, 3, 15)));
		assertTrue(pn.givDosis(LocalDate.of(2020, 3, 31)));
		assertFalse(pn.givDosis(LocalDate.of(2020, 4, 1)));
	}
}
