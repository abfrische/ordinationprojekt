package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.Laegemiddel;
import ordination.Ordination;

public class OrdinationTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void antalDageTest() {
		Ordination tc1 = new DagligFast(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 4));
		assertEquals(3, tc1.antalDage());

		Ordination tc2 = new DagligFast(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 2));
		assertEquals(1, tc2.antalDage());

		Ordination tc3 = new DagligFast(LocalDate.of(2020, 3, 4), LocalDate.of(2020, 3, 2));
		assertEquals(-1, tc3.antalDage());
	}
	
	@Test
	public void setLaegemiddelTest() {
		Laegemiddel acetylsalisylsyre = new Laegemiddel("Acetylsalisylsyre", 1.2, 2, 3.5, "stk");
		Laegemiddel paracetamol = new Laegemiddel("Paracetamol", 1.4, 2.5, 4, "stk"); 
		Ordination o = new DagligFast(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 4));
		o.setLaegemiddel(acetylsalisylsyre);
		
		assertEquals(acetylsalisylsyre, o.getLaegemiddel());
		o.setLaegemiddel(paracetamol);
		assertEquals(paracetamol, o.getLaegemiddel());
	
			
	}

}
